<?php

namespace Rizify;

class RizifyOrder
{
    public $cc_cvv;
    public $cc_yr;
    public $cc_mo;
    public $cc_number;
    public $gateway_id;
    public $mid_id;
    public $price;
    public $tax;
    public $shipping;
    public $id;
    public $transaction_id;
    public $vault_id;
    public $amount;
    public $description;
    public $redirect;
    public $products;
    public $tax_rate;
    public $retry_reference_id;
    public $order_id;
    public $subscription_id;
    public $product_name;
    public $product_id;
    public $payment_token;
    public $return_url;
    
    public $a;
    public $s1;
    public $s2;
    public $s3;
    public $campaign_id;
    public $campaign_name;
    public $adset_id;
    public $adset_name;
    public $utm_campaign;
    public $utm_adgroup;
    public $utm_ad;
    public $mobile;
    public $device;
    public $vault_add;
    public $card_add;
    public $enable_sandbox;

    public function __construct($data)
    {
        if (empty($data))
        {
            throw new \Exception("Missing order information");
        }

        foreach ($data as $prop => $d)
        {
            if (property_exists($this, $prop))
            {
                $this->{$prop} = $d;
            }
            else
            {
                throw new \Exception("Property $prop does not exist on a RizifyOrder object");
            }
        }
    }    
}
