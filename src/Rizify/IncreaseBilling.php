<?php

namespace Rizify;

use GuzzleHttp\Client;
use Rizify\RizifyOrder;
use Rizify\RizifyCustomer;

/*
 * php >=7.0 client
 */
class IncreaseBilling
{

    private $api_key;
    private $api_url = null;
    private $_customer = null;
    private $_order = null;
    private $_version = "1.4.2";

    /*
     * construct api key and api url
     */
    public function __construct($api_key = false, $api_url = false)
    {
        if (!$api_key && !$api_url)
        {
            $ini = parse_ini_file(__DIR__ . '/../../config.ini');
            if (empty($ini['api_url']))
            {
                throw new \Exception("App URL Empty");
            }

            if (empty($ini['api_key']))
            {
                throw new \Exception("API Key Empty");
            }

            $this->api_url = $ini['api_url'];
            $this->api_key = $ini['api_key'];
        }
        else
        {
            $this->api_url = $api_url;
            $this->api_key = $api_key;
        }
    }

    /*
     * charge a card
     */
    public function sale()
    {
        if (empty($this->_customer))
            return json_encode(['success' => false, 'message' => 'missing customer information']);

        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('sale', $payload, 'POST');
    }

    /*
     * gateway AI test
     */
    public function gatewayAi()
    {
        if (empty($this->_customer))
            return json_encode(['success' => false, 'message' => 'missing customer information']);

        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('gateway-ai', $payload, 'POST');
    }

    /*
     * process a void
     */
    public function void()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = $this->_order;

        return $this->send('void', $payload, 'POST');
    }

    /*
     * process a refund
     */
    public function refund()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = $this->_order;

        return $this->send('refund', $payload, 'POST');
    }

    /*
     * process a chargeback
     */
    public function chargeback()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = $this->_order;

        return $this->send('chargeback', $payload, 'POST');
    }

    /*
     * process a vault charge
     */
    public function vaultCharge()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('vault-charge', $payload, 'POST');
    }
    
    /*
     * process a vault migration
     */
    public function vaultMigrate()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('vault-migrate', $payload, 'POST');
    }

    /*
     * process a vault charge
     */
    public function vaultAi()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('vault-ai', $payload, 'POST');
    }

    /*
     * create a vault customer
     */
    public function vaultCreate()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('vault-add', $payload, 'POST');
    }
    
    /*
     * update a vault customer
     */
    public function vaultUpdate()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('vault-update', $payload, 'POST');
    }

    /*
     * import a customer
     */
    public function createCustomer()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing customer information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('customer-create', $payload, 'POST');
    }

    /*
     * import a vault customer
     */
    public function createCustomerVault()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing customer information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('customer-create-vault', $payload, 'POST');
    }

    /*
     * process a vault charge
     */
    public function validate()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('validate', $payload, 'POST');
    }

    /*
     * subscriber add
     */
    public function subscriberAdd()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('subscriber-add', $payload, 'POST');
    }

    /*
     * subscriber create
     */
    public function subscriberCreate()
    {
        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('subscriber-create', $payload, 'POST');
    }

    /*
     * subscriber create
     */
    public function subscriberGet()
    {
        if (empty($this->_customer))
            return json_encode(['success' => false, 'message' => 'missing customer information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('subscriber-get', $payload, 'POST');
    }

    /*
     * subscriber create
     */
    public function subscriberDisable()
    {
        if (empty($this->_customer))
            return json_encode(['success' => false, 'message' => 'missing customer information']);

        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('subscriber-disable', $payload, 'POST');
    }

    /*
     * setup the order object
     */
    public function order($data)
    {
        try
        {
            $o = new RizifyOrder($data);
            $this->_order = array_filter((array) $o, 'strlen');
        }
        catch (\Exception $ex)
        {
            throw new \Exception($ex->getMessage());
        }
    }

    /*
     * setup the customer object
     */
    public function customer($data)
    {
        try
        {
            $c = new RizifyCustomer($data);
            $this->_customer = array_filter((array) $c, 'strlen');
        }
        catch (Exception $ex)
        {
            throw new \Exception($ex->getMessage());
        }
    }

    /*
     * 3DS step 1
     */
    public function threeDsStepOne()
    {
        if (empty($this->_customer))
            return json_encode(['success' => false, 'message' => 'missing customer information']);

        if (empty($this->_order))
            return json_encode(['success' => false, 'message' => 'missing order information']);

        $payload = array_merge($this->_customer, $this->_order);

        return $this->send('three-ds-step-one', $payload, 'POST');
    }

    /*
     * 3DS step 1
     */
    public function threeDsConfirmation($token, $gateway_id)
    {
        $payload = array('token' => $token, 'gateway_id' => $gateway_id);

        return $this->send('three-ds-confirmation', $payload, 'POST');
    }

    /*
     * get the order
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /*
     * get the customer
     */
    public function getCustomer()
    {
        return $this->_customer;
    }

    /*
     * send the request to IncreaseBilling.com
     */
    private function send($path, $payload, $type)
    {
        try
        {
            $client = new Client();
            $response = $client->request($type, $this->api_url . $path, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->api_key,
                    'Accept' => 'application/json',
                    'User-Agent' => 'IncreaseBilling/' . $this->_version . '-php',
                ],
                'form_params' => $payload
            ]);

            return json_decode($response->getBody()->getContents());
        }
        catch (\GuzzleHttp\Exception\ConnectException $e)
        {            
            return $e->getMessage();
        }
        catch (\GuzzleHttp\Exception\RequestException $e)
        {
            return json_decode($e->getResponse()->getBody()->getContents()) ?? $e->getResponse();                  
        }
        catch (\Exception $ex)
        {         
            return $ex->getMessage();         
        }
    }
}
